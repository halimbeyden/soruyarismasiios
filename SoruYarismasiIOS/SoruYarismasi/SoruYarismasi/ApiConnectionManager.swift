//
//  ApiConnectionManager.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
import UIKit
typealias ServiceResponse = (JSON,NSError?)->Void
public class ApiConnectionManager:NSObject{
    static let sharedInstance = ApiConnectionManager()
    func getJSON(AppModule:String,parameters:[String:AnyObject] = [:], callback:(JSON) -> Void) -> Void {
        switch(AppModule) {
        case "GetExternalLogins":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Account/ExternalLogins?returnUrl=/&generateState=true",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetUserInfo":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Account/UserInfo",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "SetActiveCategory":
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Student/ActiveCategory?categoryId="+String(parameters["categoryId"]!),body: [:],onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "Logout":
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Account/Logout",body: [:],onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetAllCategories":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/General/AllCategories",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetCategoryDetails":  //categoryId
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/General/CategoryDetails?categoryId="+String(parameters["categoryId"]!),onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetTests":    //topicId
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Test/Tests?topicId="+String(parameters["topicId"]!),onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetMyName":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/GetName",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        //MARK: Duel
        case "GetTestRequests":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Duel/GetTestRequests?TestId=\(ApiContext.sharedInstance.ActiveTest.Id)",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "DuelInvite"://UserId,TestId
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Duel/Invite?UserId=\(String(parameters["UserId"]!))&TestId=\(ApiContext.sharedInstance.ActiveTest.Id)",body:parameters,onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "DuelAccept": //duelid
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Duel/Accept?duelid=\(String(parameters["duelid"]!))",body: parameters,onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "DuelDecline": //duelid
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Duel/Decline?duelid=\(String(parameters["duelid"]!))",body: parameters,onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "DuelCancelInvention": //duelid
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Duel/CancelInvitation?duelid=\(String(parameters["duelid"]!))",body: parameters,onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "DuelCancelMyInventions": //testid
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Duel/CancelMyInvitation",body: parameters,onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetDuelHistory": //
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Duel/History",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetStudents": //query
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/Search?query="+String(parameters["query"]!)+"&categoryId=\(ApiContext.sharedInstance.ActiveCategory)",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        //MARK: Test
        case "GetTestDetail": //testid
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Test/Detail?testid="+String(parameters["testid"]!),onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetTestResult": //testid
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Test/Result?testid="+String(parameters["testid"]!),onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "SendTestResults": //TestId, Rating, Answers[],Reports[]
            makeHTTPPostRequest(Connection.sharedInstance.ip + "/api/Test/Answer",body: parameters,onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "GetTestHistory":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Test/History",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        //MARK: Parent
        case "AcceptParent": //requestid
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/AcceptParent?requestid=\(String(parameters["requestid"]!))",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "DeclineParent": //requestid
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/DeclineParent?requestid=\(String(parameters["requestid"]!))",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "ParentRequests":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/ParentRequests",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "Parents":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/Parents",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "CancelParent": //parenthoodid
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/Student/CancelParent?parenthoodid=\(String(parameters["parenthoodid"]!))",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        case "AchievementDetails":
            makeHTTPGetRequest(Connection.sharedInstance.ip + "/api/General/AchievementDetails",onCompletion:{ json,err -> Void in
                callback(json as JSON)
            })
            break
        default:
            break
        }
    }
    
    func getImage(AppModule:String,parameters:[String:AnyObject] = [:], callback:(NSData) -> Void) -> Void {
        switch(AppModule) {
        case "GetQuestion": //questionid,testid,mapid
            makeHTTPGetRequestForImages(Connection.sharedInstance.ip + "/api/Test/GetQuestion?questionid="+String(parameters["questionid"]!)+"&testid="+String(parameters["testid"]!)+"&mapid="+String(parameters["mapid"]!),onCompletion:{ (data) -> Void in
                callback(data as NSData)
            })
            break
        case "GetSolution": //questionid,testid,mapid
            makeHTTPGetRequestForImages(Connection.sharedInstance.ip + "/api/Test/GetSolution?questionid="+String(parameters["questionid"]!)+"&testid="+String(parameters["testid"]!)+"&mapid="+String(parameters["mapid"]!),onCompletion:{ (data) -> Void in
                callback(data as NSData)
            })
            break
        default:
            break
        }
    }
    func makeHTTPGetRequestForImages(path: String, onCompletion: (NSData)->Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: path)!)
        request.setValue("Bearer \(Connection.sharedInstance.accessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("image/png", forHTTPHeaderField: "Content-Type")

        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            onCompletion(data!)
        })
        task.resume()
    }
    func makeHTTPGetRequest(path: String, onCompletion: ServiceResponse) {
        let request = NSMutableURLRequest(URL: NSURL(string: path.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!)
        request.setValue("Bearer \(Connection.sharedInstance.accessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            let json:JSON = JSON(data: data!)
            onCompletion(json, error)
        })
        task.resume()
    }
    func makeHTTPPostRequest(path: String, body: [String: AnyObject], onCompletion: ServiceResponse) {
        let request = NSMutableURLRequest(URL: NSURL(string: path)!)
        request.setValue("Bearer \(Connection.sharedInstance.accessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Set the method to POST
        request.HTTPMethod = "POST"
        
        // Set the POST body for the request
        do{
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(body,options: [])
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                let json:JSON = JSON(data: data!)
                onCompletion(json, error)
            })
            task.resume()
        }
        catch{
            print(error)
            return
        }
    }
    func jsonResultHandle(json:JSON, view:UIViewController,Unsuccess:()->Void = {}){
        dispatch_async(dispatch_get_main_queue(), {
            if(json["Success"].boolValue != true){
                let alertController = UIAlertController(title: "Hata Mesajı", message:
                json["ErrorMessage"].stringValue, preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Kapat", style: UIAlertActionStyle.Default,handler: nil))
                view.presentViewController(alertController, animated: true, completion: nil)
            }else{
                Unsuccess()
            }
        })
    }
}