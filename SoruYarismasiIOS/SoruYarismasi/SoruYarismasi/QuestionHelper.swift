//
//  QuestionHelper.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class QuestionHelper{
    static var ActiveQuestion = 0
    static var ImageView:UIImageView!
    static var ShapeJsonStrings:[String] = []
    static var Loading: UIActivityIndicatorView!
    static var Questions:[NSData] = []
    static var Solutions:[Int:NSData] = [:]
    static var testDetail:TestDetail = TestDetail()
    static var testResult:TestResult!
    static var UserAnswers:[Int] = []
    static var Picker:UIPickerView!
    static var QLabels:UIView!
    static let RatingTime = 5
    static let ResponseTime = 10
    static var Offsets:CGRect!
    static func initialize(){
        ActiveQuestion = 0
        cleanQuestions()
        for(var i=0;i<testDetail.MapIds.count;i++){
            UserAnswers.append(-1)
            ShapeJsonStrings.append("")
        }
        for(var i = 0;i<5;i++){
            let label:UILabel = QLabels.subviews[i] as! UILabel
            if(i==ActiveQuestion){
                label.text = String(i+1)
                label.textColor = UIColor.greenColor()
            }else{
                label.text = ""
                label.textColor = UIColor.darkGrayColor()
            }
        }

    }
    static func getOptionName(option:Int)->String{
        switch(option){
        case -1:
            return "Boş"
        case 0:
            return "A"
        case 1:
            return "B"
        case 2:
            return "C"
        case 3:
            return "D"
        case 4:
            return "E"
        default:
            return "Tanımsız"
        }
    }
    static func getQuestions(questionIndex:Int){
        if(questionIndex>=testDetail.MapIds.count || questionIndex<0){
            return
        }
        var params:[String:String] = [:]
        params["testid"] = String(ApiContext.sharedInstance.ActiveTest.Id)
        params["questionid"] = String(testDetail.TestQuestionIds[questionIndex])
        params["mapid"] = String(testDetail.MapIds[questionIndex])
        
        ApiConnectionManager.sharedInstance.getImage("GetQuestion",parameters: params) { (data) -> Void in
            Questions.append(data)
            saveQuestion("q_"+String(questionIndex), data: data,callback: {
                if(ActiveQuestion == questionIndex){
                    dispatch_async(dispatch_get_main_queue(), {
                        loadQuestion(questionIndex)
                    })
                }
                getQuestions(questionIndex+1)
            })
            
        }
    }
    static func saveQuestion(name:String,data:NSData,callback:()->Void){
        do{
            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
            let getImagePath = paths.stringByAppendingString("/"+name + ".png")
            let checkValidation = NSFileManager.defaultManager()
            if (checkValidation.fileExistsAtPath(getImagePath)){
                try checkValidation.removeItemAtPath(getImagePath)
            }
            data.writeToFile(getImagePath, atomically: true)
            callback()
        }catch{
            
        }
    }
    static func getQuestion(name:String)->UIImage?{
            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
            let getImagePath = paths.stringByAppendingString("/"+name + ".png")
            let checkValidation = NSFileManager.defaultManager()
            if (checkValidation.fileExistsAtPath(getImagePath)){
                return UIImage(contentsOfFile: getImagePath)
            }
        return nil
    }
    static func cleanQuestions(){
        var i:Int=0
        while(true){
            do{
                let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
                let imagePath = paths.stringByAppendingString("/"+"q_"+String(i) + ".png")
                let checkValidation = NSFileManager.defaultManager()
                if (checkValidation.fileExistsAtPath(imagePath)){
                    try checkValidation.removeItemAtPath(imagePath)
                    i++
                }
                else{
                    break;
                }
            }catch{
                continue
            }
        }
    }
    static func loadQuestion(questionIndex:Int){
        if let image = getQuestion("q_"+String(questionIndex)){
            UserAnswers[ActiveQuestion] = Picker.selectedRowInComponent(0)-1
            ActiveQuestion = questionIndex
            Picker.selectRow(UserAnswers[ActiveQuestion]+1, inComponent: 0, animated: true)
            ImageView.image = image
            Offsets = AVMakeRectWithAspectRatioInsideRect(ImageView.image!.size,ImageView.frame)
            if(!ShapeJsonStrings[ActiveQuestion].isEmpty){
                
            }
            Loading.stopAnimating()
            var counter:Int = 0
            for(var i = (ActiveQuestion-2);i<=(ActiveQuestion+2);i++){
                let label:UILabel = QLabels.subviews[counter++] as! UILabel
                if(i==ActiveQuestion){
                    label.text = String(i+1)
                    label.textColor = UIColor.greenColor()
                }
                else if(i>=0 && i<testDetail.MapIds.count){
                    label.text = String(i+1)
                    if(UserAnswers[i]>(-1)){
                        label.textColor = UIColor.blueColor()
                    }
                    else{
                        label.textColor = UIColor.lightGrayColor()
                    }
                }else{
                    label.text = ""
                    label.textColor = UIColor.darkGrayColor()
                }
            }
        }
    }
}