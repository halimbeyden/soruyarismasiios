//
//  AgendaTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 11/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class AgendaTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Ajanda"
        sideMenuController()?.sideMenu?.hideSideMenu()
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    @IBAction func SinavaGirisYapClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        ApiContext.sharedInstance.ActiveTest = ApiContext.sharedInstance.Agenda[(index?.row)!]
        let testLobbyViewController = storyboard!.instantiateViewControllerWithIdentifier("TestLobbyView") as! TestLobbyViewController
        presentViewController(testLobbyViewController, animated: true, completion: nil)
    }
    @IBAction func AjandadanCikarClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        yesNoDialog("Ajandadan Kaldır", message: "Bu testi ajandanızdan kaldırmak istiyor musunuz?", yesCallBack: {
            ApiContext.sharedInstance.Agenda.removeAtIndex((index?.row)!)
            PersistDataHelper.remove("Agenda")
            let list = JSON(ApiContext.sharedInstance.Agenda.map{$0.json})
            PersistDataHelper.put("Agenda", object:list.rawString()!)
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
            })
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ApiContext.sharedInstance.Agenda.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AgendaCell", forIndexPath: indexPath) as! AgendaTableViewCell
        let test:Test = ApiContext.sharedInstance.Agenda[indexPath.row]
        
        cell.Publisher.text = test.Publisher
        cell.Credit.text = String(test.Credit)
        cell.Remaining.text = test.getRemainingTimeShortVersion()

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
