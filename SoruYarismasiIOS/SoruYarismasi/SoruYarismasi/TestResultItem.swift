//
//  TestResultItem.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 07/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class TestResultItem {
    var UserName:String!
    var Score:Float!
    init(json:JSON){
        UserName = json["UserName"].stringValue
        Score = json["Score"].floatValue
    }
}