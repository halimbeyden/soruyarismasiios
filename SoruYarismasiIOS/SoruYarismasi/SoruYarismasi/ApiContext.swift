//
//  ApiContext.swift
//  SoruYarismasi
//  
//  Created by Halim Beyden on 16/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
import UIKit
class ApiContext:NSObject{
    static let sharedInstance:ApiContext = ApiContext()
    var Categories:[Category] = []
    var smtvc = SideMenuTableViewController()
    var Lectures:[Lecture] = []
    var ActiveCategory:Int = -1
    var ActiveTest:Test = Test()
    var testHistory:[TestHistory] = []
    var MyName:String = ""
    var Agenda:[Test] = []
    var Navigation:UINavigationController!
    func changeActiveCategory(ac:Int, onCompletion:()->Void){
        var params:[String:Int] = [:]
        params["categoryId"] = ac
        ApiConnectionManager.sharedInstance.getJSON("SetActiveCategory",parameters: params) { (json) -> Void in
            self.ActiveCategory = ac
            PersistDataHelper.put("ActiveCategory", object: ac)
            self.Lectures.removeAll()
            ApiConnectionManager.sharedInstance.getJSON("GetCategoryDetails",parameters: params) { (json) -> Void in
                for var i=0;i<json.count;i++ {
                    ApiContext.sharedInstance.Lectures.append(Lecture(json:json[i]))
                }
                PersistDataHelper.put("Lectures", object: json.rawString()!)
                onCompletion()
            }
        }
        
    }
    func initializePersistData(){
        if let ac=PersistDataHelper.get("ActiveCategory"){
            ActiveCategory = ac as! Int
        }
        if let ac=PersistDataHelper.get("Lectures"){
            let json = JSON.parse(ac as! String)
            if (json != nil && json != JSON(NSNull())){
                for var i=0;i<json.count;i++ {
                    Lectures.append(Lecture(json:json[i]))
                }
            }
        }
        if let ac=PersistDataHelper.get("AccessToken"){
            Connection.sharedInstance.accessToken = ac as! String
        }
        if let ac=PersistDataHelper.get("Agenda"){
            let json = JSON.parse(ac as! String)
            if (json != nil && json != JSON(NSNull())){
                for var i=0;i<json.count;i++ {
                    Agenda.append(Test(json:json[i]))
                }
            }
        }
    }
    func reset(){   //Logout
        PersistDataHelper.remove("ActiveCategory")
        PersistDataHelper.remove("AccessToken")
        Categories.removeAll()
        Lectures.removeAll()
        Connection.sharedInstance.accessToken = ""
        ActiveCategory = -1
        ApiConnectionManager.sharedInstance.getJSON("Logout") { (json) -> Void in
        }
    }
}