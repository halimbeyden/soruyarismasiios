//
//  Lecture.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
class Lecture{
    var Id:Int = 0
    var Name:String = ""
    var Order:Int = 0
    var Topics:[Topic] = []
    
    init(json:JSON){
        self.Id = json["Id"].intValue
        self.Name = json["Name"].stringValue
        self.Order = json["Order"].intValue
        for(var i=0;i<json["Topics"].count;i++){
            Topics.append(Topic(json:json["Topics"][i]))
        }
    }
    static func findLectureById(id:Int,lectures:[Lecture])->Lecture?{
        for(var i:Int=0;i<lectures.count;i++){
            if(lectures[i].Id == id){
                return lectures[i]
            }
        }
        return nil;
    }
}