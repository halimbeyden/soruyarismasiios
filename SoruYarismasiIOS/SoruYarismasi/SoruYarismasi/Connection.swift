//
//  Connection.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 09/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
class Connection {
    static let sharedInstance = Connection()
    let ip:String = "http://192.168.2.22:6199"
    var provider:Int = 0
    var providerName:String = ""
    var accessToken:String = ""
    var headers:[String:String] {
        get {
            return [
                "Authorization": "Bearer "+accessToken,
                "Content-Type": "application/json"
            ]
        }
    }
}
