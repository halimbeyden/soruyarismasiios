//
//  DuelTableViewCell.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class DuelTableViewCell: UITableViewCell {

    @IBOutlet weak var StudentNameLabel: UILabel!
    
    @IBOutlet weak var RejectButton: UIButton!
    @IBOutlet weak var AcceptButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
