//
//  DuelRequest.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class DuelRequest:NSObject {
    var Id:Int!
    var TopicId:Int!
    var TestId:Int!
    var LectureId:Int!
    var StudentId:String!
    var StudentName:String!
    var Accepted:Bool!
    var IsReceived:Bool!
    init(json:JSON) {
        Id = json["Id"].intValue
        TopicId = json["TopicId"].intValue
        TestId = json["TestId"].intValue
        LectureId = json["LectureId"].intValue
        StudentId = json["StudentId"].stringValue
        StudentName = json["StudentName"].stringValue
        Accepted = json["Accepted"].boolValue
        IsReceived = json["IsReceived"].boolValue
    }
}