//
//  MainController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit
class MainController:UIViewController,ENSideMenuDelegate {
    @IBOutlet weak var MainActivityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiContext.sharedInstance.Navigation = self.navigationController
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
}