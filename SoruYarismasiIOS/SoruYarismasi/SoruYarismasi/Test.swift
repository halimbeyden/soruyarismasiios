//
//  Test.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 18/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
class Test:NSObject {
    var Id:Int = 0
    var RemainingSeconds:Int = 0
    var Credit:Int = 0
    var LectureId:Int = 0
    var TopicId:Int = 0
    var Publisher:String = ""
    var Date:NSDate = NSDate()
    var json:JSON!
    override init(){
        
    }
    init(json:JSON) {
        self.Id = json["Id"].intValue
        self.RemainingSeconds = json["RemainingSeconds"].intValue
        self.Credit = json["Credit"].intValue
        self.LectureId = json["LectureId"].intValue
        self.TopicId = json["TopicId"].intValue
        self.Publisher = json["Publisher"].stringValue
        self.json = json
        Date = NSDate(timeIntervalSinceNow: NSTimeInterval(RemainingSeconds))
    }
    func getRemainingTimeShortVersion()->String{
        if(RemainingSeconds<=0){
            return "-";
        }
        else if(RemainingSeconds<60){
            return String(RemainingSeconds) + " saniye";
        }
        else if(RemainingSeconds<(60*60)){
            return String((RemainingSeconds/(60))) + " dakika";
        }
        else if(RemainingSeconds<(60*60*24)){
            return String((RemainingSeconds/(60*60))) + " saat";
        }
        else{
            return String((RemainingSeconds/(60*60*24))) + " gün";
        }
    }
}
