//
//  DuelInviteViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class DuelInviteViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var students:[StudentItem] = []
    var delayTimer:NSTimer!
    var lastSearchDate:NSDate = NSDate()
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = "İsim Giriniz"
        // Do any additional setup after loading the view.
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var params:[String:AnyObject] = [:]
        params["UserId"] = students[indexPath.row].StudentId
        yesNoDialog("Düello Daveti", message: "\(students[indexPath.row].StudentName) kullanıcısını düelloya davet etmek istediğinize emin misiniz?", yesCallBack: {
            ApiConnectionManager.sharedInstance.getJSON("DuelInvite", parameters: params) { (json) -> Void in
                ApiConnectionManager.sharedInstance.jsonResultHandle(json, view: self){
                    Void in
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // # Incomplete implementation, return the number of rows
        return students.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        cell.textLabel?.text = self.students[indexPath.row].StudentName
        return cell
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText.characters.count>=2){
            if(delayTimer != nil){
                delayTimer.invalidate()
            }
            delayTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("getStudents:"), userInfo: searchText, repeats: false)
            
        }else if(searchText.characters.count==0){
            students.removeAll()
        }
    }
    func getStudents(timer : NSTimer) {
        let searchText = timer.userInfo as! String
        print(searchText)
        var params:[String:AnyObject] = [:]
        params["query"] = searchText
        ApiConnectionManager.sharedInstance.getJSON("GetStudents", parameters: params, callback: { (json) -> Void in
            self.students.removeAll()
            for var i=0;i<json.count;i++ {
                self.students.append(StudentItem(json: json[i]))
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
                });
        })
        // here code to perform
    }
    override func performSegueWithIdentifier(identifier: String, sender: AnyObject?) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
