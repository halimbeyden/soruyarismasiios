//
//  ResultsViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var ResultsTable: UITableView!
    @IBOutlet weak var ScoreLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        ResultsTable.delegate = self
        ResultsTable.dataSource = self
        
        ScoreLabel.text = "\(QuestionHelper.testResult.TrueCount)d \(QuestionHelper.testResult.FalseCount)y \(QuestionHelper.testResult.EmptyCount)b Skor: " + (NSString(format: "%.2f", QuestionHelper.testResult.Score) as String)
        // Do any additional setup after loading the view.
    }

    @IBAction func MainMenuClick(sender: UIBarButtonItem) {
        yesNoDialog("Ana Menüye Dön", message: "Ana menüye dönmek istiyor musunuz?", yesCallBack: {
            let storyboard = UIStoryboard(name: "MainActivity", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("MainController") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return QuestionHelper.testResult.Results.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ResultsTableViewCell", forIndexPath: indexPath) as! ResultsTableViewCell
        cell.ScoreLabel.text = String(format:"%.2f",QuestionHelper.testResult.Results[indexPath.row].Score)
        cell.NameLabel.text = QuestionHelper.testResult.Results[indexPath.row].UserName
        cell.SiraLabel.text = String(indexPath.row+1)+"."
        return cell
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
