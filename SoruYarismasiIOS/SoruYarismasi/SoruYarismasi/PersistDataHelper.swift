//
//  PersistDataHelper.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class PersistDataHelper {
    static let ArchiveUrl:String = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
    static func get(key:String)->AnyObject?{
        do{
            return try NSUserDefaults.standardUserDefaults().objectForKey(key)
        } catch{
            fatalError("getError \(error)")
        }
        
    }
    static func put(key:String,object:AnyObject){
        NSUserDefaults.standardUserDefaults().setObject(object, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    static func remove(key:String){
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}