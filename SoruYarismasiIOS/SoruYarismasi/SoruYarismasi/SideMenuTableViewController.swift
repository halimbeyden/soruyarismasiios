//
//  SideMenuTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class SideMenuTableViewController: UITableViewController {
    var selectedMenuItem : Int = 0
    let menuItems:[String] = ["Ana Ekran","Başarılarım","Düellolarım","Sınav Sonuçlarım","Ajandam","Dükkan","Evebeynler","İsim Değiştir","Çıkış Yap"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.clearColor()
        tableView.scrollsToTop = false
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuItems.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
        if (cell == nil) {
            cell = SideMenuTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.darkGrayColor()
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }
        cell!.textLabel?.text = menuItems[indexPath.row]

        return cell!
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //if (indexPath.row == selectedMenuItem) {
          //  return
        //}
        let mainStoryboard = UIStoryboard(name: "MainActivity", bundle: nil)
        selectedMenuItem = indexPath.row
        switch (indexPath.row) {
        case 0:
            dispatch_async(dispatch_get_main_queue(), {
                self.sideMenuController()?.sideMenu?.hideSideMenu()
                ApiContext.sharedInstance.Navigation.popToRootViewControllerAnimated(true)
                })
            break
        case 1:
            let destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("AchivementView") as! AchivementCollectionViewController
            dispatch_async(dispatch_get_main_queue(), {
                self.sideMenuController()?.sideMenu?.hideSideMenu()
                ApiContext.sharedInstance.Navigation.pushViewController(destViewController, animated: true)
            })
            break
        case 2:
            let destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("DuelHistoryView") as! DuelHistoryTableViewController
            dispatch_async(dispatch_get_main_queue(), {
                self.sideMenuController()?.sideMenu?.hideSideMenu()
                ApiContext.sharedInstance.Navigation.pushViewController(destViewController, animated: true)
            })
            break
        case 3:
                let destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ExamHistoryView") as! ExamResultsTableViewController
                dispatch_async(dispatch_get_main_queue(), {
                    self.sideMenuController()?.sideMenu?.hideSideMenu()
                    ApiContext.sharedInstance.Navigation.pushViewController(destViewController, animated: true)

                })
            break
        case 4:
            let destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("AgendaView") as! AgendaTableViewController
            dispatch_async(dispatch_get_main_queue(), {
                self.sideMenuController()?.sideMenu?.hideSideMenu()
                ApiContext.sharedInstance.Navigation.pushViewController(destViewController, animated: true)
                
            })
            break
        case 6:
            let destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ParentView") as! ParentTableViewController
            dispatch_async(dispatch_get_main_queue(), {
                self.sideMenuController()?.sideMenu?.hideSideMenu()
                ApiContext.sharedInstance.Navigation.pushViewController(destViewController, animated: true)
            })
            break
        case 8:
            yesNoDialog("Çıkış Yap", message: "Çıkış yapmak istediğinizden emin misiniz?", yesCallBack: {
                ApiContext.sharedInstance.reset()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("MainNav") as! UINavigationController
                self.presentViewController(vc, animated: true, completion: nil)
                })
            break
        default:
            //destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainView")
            break
        }
        
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
