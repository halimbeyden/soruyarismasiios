//
//  ExamResultsTableViewCell.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class ExamResultsTableViewCell: UITableViewCell {

    @IBOutlet weak var DersKonuLabel: UILabel!
    @IBOutlet weak var YayineviLabel: UILabel!
    @IBOutlet weak var SkorLabel: UILabel!
    @IBOutlet weak var TarihLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
