//
//  Achivement.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
enum AchievementNames:String{
    case b10
    case b10g
}
class Achievement {
    var Id:Int!
    var Name:String!
    var Information:String!
    var GainCount:Int!
    var ImageKey:String!
    
    init(json:JSON){
        Id = json["Id"].intValue
        Name = json["Name"].stringValue
        Information = json["Information"].stringValue
        GainCount = json["GainCount"].intValue
        ImageKey = json["ImageKey"].stringValue
    }
    
    
    
    static func getName()->String{
        return AchievementNames.b10.rawValue
    }
}