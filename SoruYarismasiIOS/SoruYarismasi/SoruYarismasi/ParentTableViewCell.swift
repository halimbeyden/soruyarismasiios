//
//  ParentTableViewCell.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 11/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class ParentTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var ParentName: UILabel!

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
