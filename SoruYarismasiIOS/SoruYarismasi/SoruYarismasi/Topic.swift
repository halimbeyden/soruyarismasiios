//
//  Topic.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
class Topic {
    var Id:Int = 0
    var Name:String = ""
    var Order:Int = 0
    init(json:JSON){
        self.Id = json["Id"].intValue
        self.Name = json["Name"].stringValue
        self.Order = json["Order"].intValue
    }
    static func findTopicById(id:Int,topics:[Topic])->Topic?{
        for(var i:Int = 0;i<topics.count;i++){
            if(topics[i].Id == id){
                return topics[i]
            }
        }
        return nil;
    }
}