//
//  DuelHistoryTableViewCell.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 18/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class DuelHistoryTableViewCell: UITableViewCell {
    //me vs username
    //lecture - topic
    //score - opponent score
    //minutes passed
    @IBOutlet weak var OScoreLabel: UILabel!
    @IBOutlet weak var ONameLabel: UILabel!
    @IBOutlet weak var MyScoreLabel: UILabel!
    @IBOutlet weak var MyNameLabel: UILabel!
    @IBOutlet weak var DersKonuLabel: UILabel!
    @IBOutlet weak var TarihLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
