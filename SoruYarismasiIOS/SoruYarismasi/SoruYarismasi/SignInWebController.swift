//
//  SignInWebController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 09/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit
class SignInWebController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var signInWebView: UIWebView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var flag = false
    override func viewDidLoad() {
        super.viewDidLoad()
        signInWebView.delegate = self
        SwiftSpinner.show("Sayfa yükleniyor")
        self.automaticallyAdjustsScrollViewInsets = false
        ApiConnectionManager.sharedInstance.getJSON("GetExternalLogins") { (json) -> Void in
            let url = NSURL(string: Connection.sharedInstance.ip + (json[Connection.sharedInstance.provider]["Url"].stringValue))
            let requestObj = NSURLRequest(URL:url!)
            self.signInWebView.loadRequest(requestObj)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url:String = (request.URL?.absoluteString)!
        signInWebView.hidden = true
        if(url.containsString(Connection.sharedInstance.providerName)){
            signInWebView.hidden = false
        }else if(flag==true){
            SwiftSpinner.show("Uygulamaya Giriş Yapılıyor")
        }else{
            flag = true
        }
        return true
    }
    func webViewDidStartLoad(webView: UIWebView){
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
        let url:String = (signInWebView.request?.URL?.absoluteURL.absoluteString)!
        SwiftSpinner.hide()
        if (url.containsString("access_token") == true){
            SwiftSpinner.show("Uygulamaya Giriş Yapılıyor")
            let matches = url.regex("([?#]access_token=|^((?![?#]access_token=).)+$)([^&]*)")
            Connection.sharedInstance.accessToken =  (matches[0] as NSString).substringFromIndex(14)
            PersistDataHelper.put("AccessToken", object: Connection.sharedInstance.accessToken)
            ApiConnectionManager.sharedInstance.getJSON("GetUserInfo", callback: { (json) -> Void in
                if(json["HasRegistered"].boolValue){
                    ApiConnectionManager.sharedInstance.getJSON("GetMyName", callback: { (json) -> Void in
                        ApiContext.sharedInstance.MyName = json.stringValue
                        dispatch_async(dispatch_get_main_queue(), {
                            let vc = self.storyboard!.instantiateViewControllerWithIdentifier("CategoryView") as! CategoriesController
                                self.navigationController?.pushViewController(vc, animated: true)
                            });
                    })
                }
            })
        }
    }
    func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}
