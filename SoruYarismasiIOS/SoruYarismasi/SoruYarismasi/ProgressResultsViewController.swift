//
//  ProgressResultsViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 04/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class ProgressResultsViewController: UIViewController {

    private var activityView = ActivityView()
    private var counter:Int = 0
    init(title: String,rateExist:Bool) {
        super.init(nibName: nil, bundle: nil)
        modalTransitionStyle = .CrossDissolve
        modalPresentationStyle = .OverFullScreen
        activityView.titleLabel.text = title
        view = activityView
        activityView.progressView.progress = 0
        activityView.rateExists = rateExist
        activityView.messageLabel.text = "                      "
        
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func changeMessage(message:String){
        activityView.messageLabel.text = message
    }
    func setMaxProgress(maxProgress:Int){
        activityView.maxProgress = maxProgress
    }
    func setProgress(progress:Float,message:String){
        activityView.progressView.progress = (Float(activityView.maxProgress) - progress)/Float(activityView.maxProgress)
        activityView.messageLabel.text = message
    }
    func setRateExist(val:Bool){
        activityView.floatRatingView.hidden = !val
    }
    func getRating()->Float{
        return activityView.floatRatingView.rating
    }
    func changeState(maxProgress:Int){
        activityView.progressView.progress = 0
        activityView.floatRatingView.hidden = true
        activityView.maxProgress = maxProgress
    }
}


private class ActivityView: UIView {
    
    let progressView = UIProgressView(progressViewStyle: .Bar)
    let boundingBoxView = UIView(frame: CGRectZero)
    let titleLabel = UILabel(frame: CGRectZero)
    let messageLabel = UILabel(frame: CGRectZero)
    let floatRatingView = FloatRatingView(frame: CGRectZero)
    var maxProgress:Int = 100
    var rateExists = true
    init() {
        super.init(frame: CGRectZero)
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        
        boundingBoxView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        boundingBoxView.layer.cornerRadius = 12.0
        
        progressView.progress = 0
        messageLabel.text = maxProgress.toStringSecondsToLongVersion()
        
        titleLabel.font = UIFont.boldSystemFontOfSize(UIFont.labelFontSize())
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.textAlignment = .Center
        titleLabel.shadowColor = UIColor.blackColor()
        titleLabel.shadowOffset = CGSizeMake(0.0, 1.0)
        titleLabel.numberOfLines = 0
        
        messageLabel.font = UIFont.boldSystemFontOfSize(UIFont.labelFontSize())
        messageLabel.textColor = UIColor.whiteColor()
        messageLabel.textAlignment = .Center
        messageLabel.shadowColor = UIColor.blackColor()
        messageLabel.shadowOffset = CGSizeMake(0.0, 1.0)
        messageLabel.numberOfLines = 0
        
        // Required float rating view params
        self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.contentMode = UIViewContentMode.ScaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 0
        self.floatRatingView.rating = 0
        self.floatRatingView.editable = true
        
        addSubview(boundingBoxView)
        addSubview(titleLabel)
        addSubview(progressView)
        addSubview(messageLabel)
        if(rateExists){
            addSubview(floatRatingView)
        }
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        boundingBoxView.frame.size.width = frame.size.width-30.0
        boundingBoxView.frame.size.height = frame.size.height/2.0
        boundingBoxView.frame.origin.x = ceil((bounds.width / 2.0) - (boundingBoxView.frame.width / 2.0))
        boundingBoxView.frame.origin.y = ceil((bounds.height / 2.0) - (boundingBoxView.frame.height / 2.0))
        
        let titleLabelSize = titleLabel.sizeThatFits(CGSizeMake(frame.size.width - 30.0 - 20.0 * 2.0, CGFloat.max))
        titleLabel.frame.size.width = titleLabelSize.width
        titleLabel.frame.size.height = titleLabelSize.height
        titleLabel.frame.origin.x = ceil((bounds.width / 2.0) - (titleLabel.frame.width / 2.0))
        titleLabel.frame.origin.y = ceil(progressView.frame.origin.y - progressView.frame.size.height - ((boundingBoxView.frame.height - progressView.frame.height) / 4.0) + (titleLabel.frame.height / 2.0))
        
        progressView.frame.origin.x = ceil((bounds.width / 2.0) - (progressView.frame.width / 2.0))
        progressView.frame.origin.y = ceil((bounds.height / 2.0) - (progressView.frame.height / 2.0))
        
        let messageLabelSize = messageLabel.sizeThatFits(CGSizeMake(frame.size.width - 30.0 - 20.0 * 2.0, CGFloat.max))
        messageLabel.frame.size.width = messageLabelSize.width
        messageLabel.frame.size.height = messageLabelSize.height
        messageLabel.frame.origin.x = ceil((bounds.width / 2.0) - (messageLabel.frame.width / 2.0))
        messageLabel.frame.origin.y = ceil(progressView.frame.origin.y + progressView.frame.size.height + ((boundingBoxView.frame.height - progressView.frame.height) / 4.0) - (messageLabel.frame.height / 2.0))
        if(rateExists){
            floatRatingView.frame.size.width = frame.size.width/2
            floatRatingView.frame.size.height = boundingBoxView.frame.height/4.0
            floatRatingView.frame.origin.x = ceil((bounds.width / 2.0) - (floatRatingView.frame.width / 2.0))
            floatRatingView.frame.origin.y = messageLabel.frame.origin.y + messageLabel.frame.size.height/2 + 10.0
        }
        
    }
}
