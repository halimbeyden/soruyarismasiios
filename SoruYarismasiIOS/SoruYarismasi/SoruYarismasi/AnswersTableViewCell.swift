//
//  AnswersTableViewCell.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 07/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class AnswersTableViewCell: UITableViewCell {

    @IBOutlet weak var StudentAnswerLabel: UILabel!
    @IBOutlet weak var CorrectAnswerLabel: UILabel!
    @IBOutlet weak var SoruSayisiLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
