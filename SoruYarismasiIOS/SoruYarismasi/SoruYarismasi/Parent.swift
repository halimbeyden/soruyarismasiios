//
//  Parent.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 11/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class Parent {
    var ParentId:Int! //String olması gerekmiyor mu
    var ParenthoodId:Int!
    var ParentName:String!
    var Requested:Bool!
    init(json:JSON){
        ParentId = json["ParentId"].intValue
        ParenthoodId = json["ParenthoodId"].intValue
        ParentName = json["ParentName"].stringValue
        Requested = json["RequestId"].boolValue
    }
}