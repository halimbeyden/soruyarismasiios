//
//  Extentions.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
import UIKit
extension String {
    func regex (pattern: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions(rawValue: 0))
            let nsstr = self as NSString
            let all = NSRange(location: 0, length: nsstr.length)
            var matches : [String] = [String]()
            regex.enumerateMatchesInString(self, options: NSMatchingOptions(rawValue: 0), range: all) {
                (result : NSTextCheckingResult?, _, _) in
                if let r = result {
                    let result = nsstr.substringWithRange(r.range) as String
                    matches.append(result)
                }
            }
            return matches
        } catch {
            return [String]()
        }
    }
}
extension NSDate {
    func minus (minus: NSDate,unit:NSCalendarUnit) -> NSDateComponents {
            let calender = NSCalendar.currentCalendar()
            let components = calender.components(unit,fromDate:minus,toDate:self,options: [])
            return components
    }
    func toStringLongVersion()->String{
        var result:String="";
        var rSeconds:Int = self.minus(NSDate(), unit: [.Second]).second
        if(rSeconds/(60*60)>=1){
            result+=String(rSeconds/(60*60)) + " sa ";
            rSeconds%=(60*60);
        }
        if(rSeconds/(60)>=1){
            result+=String(rSeconds/(60)) + " dk ";
            rSeconds%=(60);
        }
        if(rSeconds>0){
            result+=String(rSeconds) + " sn ";
        }
        return result;
    }
}
extension Int{
    func toStringSecondsToLongVersion()->String{
        var result:String="";
        var rSeconds = self
        if(rSeconds/(60*60)>=1){
            result+=String(rSeconds/(60*60)) + " sa ";
            rSeconds%=(60*60);
        }
        if(rSeconds/(60)>=1){
            result+=String(rSeconds/(60)) + " dk ";
            rSeconds%=(60);
        }
        if(rSeconds>0){
            result+=String(rSeconds) + " sn ";
        }
        return result;
    }
    func toStringMinutesShortVersion()->String{
        if(self<60){
            return String(self) + " dakika önce"
        }
        if(self<60*24){
            return String(self/60) + " saat önce"
        }
        if(self<60*24*30){
            return String(self/(60*24))+" gün önce"
        }
        if(self<60*24*30*12){
            return String(self/(60*24*30)) + " ay önce"
        }
        return String(self/(60*24*30*12)) + " yıl önce"
    }
}
extension UIView
{
    func copyView() -> AnyObject
    {
        return NSKeyedUnarchiver.unarchiveObjectWithData(NSKeyedArchiver.archivedDataWithRootObject(self))!
    }
}
extension UIViewController{
    func yesNoDialog(title:String,message:String,yesCallBack:()->Void,noCallBack:(()->Void) = {}){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Evet", style: .Default, handler: { (action: UIAlertAction!) in
            yesCallBack()
        }))
        
        alert.addAction(UIAlertAction(title: "Hayır", style: .Default, handler: { (action: UIAlertAction!) in
                noCallBack()
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
extension UIImage {
    func getPixelColor(pos: CGPoint) -> UIColor {
        
        let pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
        
        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }  
}
extension CGRect {
    func expandRect(offset:CGFloat)->CGRect{
        return CGRectMake(self.origin.x - offset, self.origin.y - offset, self.width + offset, self.height + offset)
    }
    func convertReggessedPoint(point:CGPoint)->CGPoint{
        let x = (point.x - self.origin.x)/self.width
        let y = (point.y - self.origin.y)/self.height
        return CGPointMake(x, y)
    }
    func convertFromRegressedPoint(point:CGPoint)->CGPoint{
        let x = point.x * self.width + self.origin.x
        let y = point.y * self.height + self.origin.y
        return CGPointMake(x,y)
    }
}
extension CGPoint {
    func multiply(multiplier:CGFloat)->CGPoint{
        return CGPoint(x: self.x * multiplier, y: self.y * multiplier)
    }
}

