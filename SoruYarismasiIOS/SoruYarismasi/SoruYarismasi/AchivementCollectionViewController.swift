//
//  AchivementCollectionViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

private let reuseIdentifier = "AchivementCell"

class AchivementCollectionViewController: UICollectionViewController {
    var ahv:AchievementDetail!
    var achievements:[Achievement] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = UIColor.whiteColor()
        self.title = "Başarılarım"
        SwiftSpinner.show("Bilgileriniz Alınıyor..")
        ApiConnectionManager.sharedInstance.getJSON("AchievementDetails") { (json) -> Void in
            self.ahv = AchievementDetail(json: json)
            self.achievements = self.ahv.Achievements.filter{$0.ImageKey != ""}
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView?.reloadData()
                SwiftSpinner.hide()
            })
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return achievements.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! AchivementCollectionViewCell
        let exist = ahv.StudentAchievements.contains(achievements[indexPath.row].Id)
        let imageKey = (exist ? achievements[indexPath.row].ImageKey : achievements[indexPath.row].ImageKey + "g")
        cell.AchivementImage.image = UIImage(named: imageKey)
        // Configure the cell
    
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let numberOfCellInRow : Int = 6
        let padding : Int = 3
        let collectionCellWidth : CGFloat = (self.view.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth , height: collectionCellWidth)
    }
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let ach = achievements[indexPath.row]
        let alert = UIAlertController()
        alert.title = ach.Name
        alert.message = ach.Information
        //let exist = ahv.StudentAchievements.contains(ach.Id)
        //let imageKey = (exist ? ach.ImageKey : ach.ImageKey + "g")
        
        //let imageView = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        //imageView.image = UIImage(named: imageKey)
        //alert.view.addSubview(imageView)
        let OKAction = UIAlertAction(title: "Tamam", style: .Default, handler: nil)
        alert.addAction(OKAction)
        self.presentViewController(alert, animated: true, completion: nil)

    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
