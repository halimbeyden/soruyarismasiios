//
//  LectureTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 18/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit

class LectureTableViewController: UITableViewController, ENSideMenuDelegate {
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
        if(ApiContext.sharedInstance.Lectures.count==0){
            var parameters:[String:String] = [:]
            parameters["categoryId"] = String(ApiContext.sharedInstance.ActiveCategory)
            ApiConnectionManager.sharedInstance.getJSON("GetCategoryDetails",parameters: parameters) { (json) -> Void in
                for var i=0;i<json.count;i++ {
                    ApiContext.sharedInstance.Lectures.append(Lecture(json:json[i]))
                }
                PersistDataHelper.put("Lectures", object: json.rawString()!)
            }
        }
    }
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ApiContext.sharedInstance.Lectures.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LectureTableViewCell", forIndexPath: indexPath) as! LectureTableViewCell
        
        let lec = ApiContext.sharedInstance.Lectures[indexPath.row]
        
        cell.LectureLabel.text = lec.Name
        
        return cell
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "TopicSegue"){
            let topicTableViewController = segue.destinationViewController as! TopicTableViewController
            
            if let selectedCell = sender as? LectureTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedCell)!
                let selectedMeal = ApiContext.sharedInstance.Lectures[indexPath.row]
                topicTableViewController.topics = selectedMeal.Topics
            }
        }
    }

}
