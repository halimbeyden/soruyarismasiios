//
//  CategoriesController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit
import Dispatch
class CategoriesController:UITableViewController {
    var categories = [Category]()
    
    override func viewWillAppear(animated: Bool) {
        SwiftSpinner.show("Bilgiler Alınıyor")
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        ApiConnectionManager.sharedInstance.getJSON("GetAllCategories") { (json) -> Void in
            for var i=0;i<json.count;i++ {
                self.categories.append(Category(id: json[i]["Id"].intValue,name: json[i]["Name"].stringValue,lo: json[i]["ListOrder"].intValue))
            }
            self.tableView.reloadData()
            SwiftSpinner.hide()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        SwiftSpinner.show("Bilgileriniz Güncelleniyor..")
        ApiContext.sharedInstance.changeActiveCategory(categories[indexPath.row].Id){
            let storyboard = UIStoryboard(name: "MainActivity", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("MainController") as! MyNavigationController
            self.presentViewController(vc, animated: true, completion: nil)
            SwiftSpinner.hide()
        }

    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "CategoryTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! CategoryTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let cat = categories[indexPath.row]
        
        cell.CategoryLabel.text = cat.Name
        
        return cell
    }
}