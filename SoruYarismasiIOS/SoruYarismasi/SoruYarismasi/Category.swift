//
//  Category.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
class Category {
    var Id:Int = 0
    var Name:String = ""
    var ListOrder:Int = 0
    init(id:Int,name:String,lo:Int){
        self.Id = id
        self.Name = name
        self.ListOrder = lo
    }
}
