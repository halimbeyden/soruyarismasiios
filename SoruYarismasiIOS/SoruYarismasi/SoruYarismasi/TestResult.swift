//
//  TestResult.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 07/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class TestResult {
    var Score:Float!
    var FalseCount:Int!
    var EmptyCount:Int!
    var TrueCount:Int!
    var GainedAchievementIds:[Int] = []
    var CorrectAnswers:[Int] = []
    var SolutionCredits:[Int] = []
    var UserName:String!
    var Results:[TestResultItem] = []
    var DuelDetail:DuelDetailItem!
    
    init(result:JSON){
        UserName = result["UserName"].stringValue
        Score = result["Score"].floatValue
        FalseCount = result["FalseCount"].intValue
        EmptyCount = result["EmptyCount"].intValue
        TrueCount = result["TrueCount"].intValue
        for i in result["GainedAchievementIds"]{
            GainedAchievementIds.append(i.1.intValue)
        }
        for i in result["CorrectAnswers"]{
            CorrectAnswers.append(i.1.intValue)
        }
        for i in result["SolutionCredits"]{
            SolutionCredits.append(i.1.intValue)
        }
        for i in result["Results"]{
            Results.append(TestResultItem(json: i.1))
        }
        DuelDetail = DuelDetailItem(json:result["DuelDetail"])
    }
}