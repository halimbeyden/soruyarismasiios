//
//  DuelHistoryTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 18/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class DuelHistoryTableViewController: UITableViewController, ENSideMenuDelegate{
    var duelDetailItems:[DuelDetailItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        SwiftSpinner.show("Bilgileriniz Alınıyor..")
        sideMenuController()?.sideMenu?.hideSideMenu()
        self.sideMenuController()?.sideMenu?.delegate = self
        ApiConnectionManager.sharedInstance.getJSON("GetDuelHistory") { (json) -> Void in
            for var i=0;i<json.count;i++ {
                self.duelDetailItems.append(DuelDetailItem(json: json[i]))
            }
            self.tableView.reloadData()
            sleep(2)
            SwiftSpinner.hide()
        }
        // Uncomment the followin g line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(animated: Bool) {
        debugPrint("viewWillAppear")
    }
    @IBAction func ToggleSideMenu(sender: UIBarButtonItem) {
        toggleSideMenuView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return duelDetailItems.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DuelHistoryCell", forIndexPath: indexPath) as! DuelHistoryTableViewCell
        let duelItem = duelDetailItems[indexPath.row]
        let lecture = Lecture.findLectureById(duelItem.LectureId, lectures: ApiContext.sharedInstance.Lectures)
        cell.DersKonuLabel.text = (lecture?.Name)!+" - "+(Topic.findTopicById(duelItem.TopicId, topics: (lecture?.Topics)!)?.Name)!
        cell.TarihLabel.text = duelItem.MinutesPassed.toStringMinutesShortVersion()
        cell.MyScoreLabel.text = String(format: "%.2f",duelItem.Score)
        cell.MyNameLabel.text = ApiContext.sharedInstance.MyName
        cell.OScoreLabel.text = String(format: "%.2f", duelItem.OpponentScore)
        cell.ONameLabel.text = duelItem.Username
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
