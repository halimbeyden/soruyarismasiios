//
//  TestTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 18/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit

class TestTableViewController: UITableViewController, ENSideMenuDelegate{
    var topicId:Int = -1
    var tests:[Test] = []
    func refresh(sender:AnyObject)
    {
        tests.removeAll()
        var parameters:[String:String] = [:]
        parameters["topicId"] = String(topicId)
        ApiConnectionManager.sharedInstance.getJSON("GetTests",parameters: parameters) { (json) -> Void in
            for var i=0;i<json.count;i++ {
                self.tests.append(Test(json: json[i]))
            }
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
    @IBAction func AddtoAgendaClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        let cell = tableView.cellForRowAtIndexPath(index!) as! TestTableViewCell
        if(sender.tag == 0){
            yesNoDialog("Ajandaya Ekle", message: "Bu testi ajandanıza eklemek istiyor musunuz?", yesCallBack: {
                ApiContext.sharedInstance.Agenda.append(self.tests[(index?.row)!])
                dispatch_async(dispatch_get_main_queue(), {
                    cell.AjandayaEkle.setTitle("Ajandadan Çıkar", forState: .Normal)
                    cell.AjandayaEkle.tag = 1
                    cell.AjandayaEkle.setTitleColor(UIColor.redColor(), forState: .Normal)
                })
                PersistDataHelper.remove("Agenda")
                let list = JSON(ApiContext.sharedInstance.Agenda.map{$0.json})
                PersistDataHelper.put("Agenda", object:list.rawString()!)
            })
        }else{
            yesNoDialog("Ajandadan Kaldır", message: "Bu testi ajandanızdan kaldırmak istiyor musunuz?", yesCallBack: {
                ApiContext.sharedInstance.Agenda = ApiContext.sharedInstance.Agenda.filter(){$0.Id != self.tests[(index?.row)!].Id}
                
                dispatch_async(dispatch_get_main_queue(), {
                    cell.AjandayaEkle.setTitle("Ajandaya Ekle", forState: .Normal)
                    cell.AjandayaEkle.tag = 1
                    cell.AjandayaEkle.setTitleColor(cell.PublisherLabel.textColor, forState: .Normal)
                })
                PersistDataHelper.remove("Agenda")
                let list = JSON(ApiContext.sharedInstance.Agenda.map{$0.json})
                PersistDataHelper.put("Agenda", object:list.rawString()!)

            })
        }
        
    }
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    @IBAction func SinavaGirisYapClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        ApiContext.sharedInstance.ActiveTest = tests[(index?.row)!]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        self.refreshControl = UIRefreshControl()
        //self.refreshControl!.attributedTitle = NSAttributedString(string: "Yenilemek İçin Aşağı Kaydırın")
        self.refreshControl!.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl!)
    }
    
    override func viewWillAppear(animated: Bool) {
        SwiftSpinner.show("Test listesi alınıyor..")
        self.tests.removeAll()
        var parameters:[String:String] = [:]
        parameters["topicId"] = String(topicId)
        ApiConnectionManager.sharedInstance.getJSON("GetTests",parameters: parameters) { (json) -> Void in
            for var i=0;i<json.count;i++ {
                self.tests.append(Test(json: json[i]))
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
                SwiftSpinner.hide()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tests.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TestTableViewCell", forIndexPath: indexPath) as! TestTableViewCell
        let test = tests[indexPath.row]
        cell.PublisherLabel.text = test.Publisher
        cell.RemainingTimeLabel.text = test.getRemainingTimeShortVersion()
        cell.CreditLabel.text = String(test.Credit) + " kredi"
        cell.AjandayaEkle.tag = 0
        if(ApiContext.sharedInstance.Agenda.map{$0.Id}.contains(test.Id)){
            cell.AjandayaEkle.setTitle("Ajandadan Çıkar", forState: .Normal)
            cell.AjandayaEkle.tag = 1
            cell.AjandayaEkle.setTitleColor(UIColor.redColor(), forState: .Normal)
        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    
    // Override to support conditional rearranging of the table view.
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
