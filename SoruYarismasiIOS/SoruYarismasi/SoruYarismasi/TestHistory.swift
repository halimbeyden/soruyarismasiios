//
//  TestHistory.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class TestHistory {
    var LectureId:Int!
    var TopicId:Int!
    var MinutesPassed:Int!
    var Score:Float!
    var Siralama:Int!
    var Publisher:String!
    init(json:JSON){
        LectureId = json["LectureId"].intValue
        TopicId = json["TopicId"].intValue
        MinutesPassed = json["MinutesPassed"].intValue
        Score = json["Score"].floatValue
        Siralama = json["Place"].intValue
        Publisher = json["Publisher"].stringValue

    }
}