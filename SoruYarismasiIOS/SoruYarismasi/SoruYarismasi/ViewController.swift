//
//  ViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 09/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ApiContext.sharedInstance.initializePersistData()
        if(Connection.sharedInstance.accessToken != ""){ //Access token exists
            if(ApiContext.sharedInstance.ActiveCategory<=0){ //ActiveCategory is not selected
                let vc = self.storyboard!.instantiateViewControllerWithIdentifier("CategoryView") as! CategoriesController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "MainActivity", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("MainController") as! MyNavigationController
                self.presentViewController(vc, animated: true, completion: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func googleClick(sender: UIButton) {
        Connection.sharedInstance.provider = 0
        Connection.sharedInstance.providerName = "google.com"
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("SignInView") as! SignInWebController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func facebookClick(sender: UIButton) {
        Connection.sharedInstance.provider = 1
        Connection.sharedInstance.providerName = "facebook.com"
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("SignInView") as! SignInWebController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func twitterClick(sender: UIButton) {
        Connection.sharedInstance.provider = 2
        Connection.sharedInstance.providerName = "twitter.com"
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("SignInView") as! SignInWebController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func microsoftClick(sender: UIButton) {
        Connection.sharedInstance.provider = 3
        Connection.sharedInstance.providerName = "microsoft.com"
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("SignInView") as! SignInWebController
        self.navigationController?.pushViewController(vc, animated: true)

    }
}

