//
//  StudentItem.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class StudentItem {
    var StudentName:String!
    var StudentId:String!
    init(json:JSON){
        StudentName = json["StudentName"].stringValue
        StudentId = json["StudentId"].stringValue
    }
}