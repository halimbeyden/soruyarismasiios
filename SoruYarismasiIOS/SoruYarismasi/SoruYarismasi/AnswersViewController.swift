//
//  AnswersViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class AnswersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var AnswersTable: UITableView!
    var selectedRow:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        AnswersTable.dataSource = self
        AnswersTable.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return QuestionHelper.testResult.CorrectAnswers.count
    }
    
    @IBAction func GetSolutionClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:AnswersTable)
        let index = AnswersTable.indexPathForRowAtPoint(btnPos)
        selectedRow = (index?.row)!
    }
    
    @IBAction func MainMenuClick(sender: UIBarButtonItem) {
        yesNoDialog("Ana Menüye Dön", message: "Ana menüye dönmek istiyor musunuz?", yesCallBack: {
            let storyboard = UIStoryboard(name: "MainActivity", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("MainController") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)

            })
        }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AnswersTableViewCell", forIndexPath: indexPath) as! AnswersTableViewCell
        cell.StudentAnswerLabel.text = "Cevabın: " + QuestionHelper.getOptionName(QuestionHelper.UserAnswers[indexPath.row])
        cell.CorrectAnswerLabel.text = "Doğru Şık: " + QuestionHelper.getOptionName(QuestionHelper.testResult.CorrectAnswers[indexPath.row])
        cell.SoruSayisiLabel.text = String(indexPath.row+1)+". Soru"
        if QuestionHelper.UserAnswers[indexPath.row] == QuestionHelper.testResult.CorrectAnswers[indexPath.row]{
            cell.StudentAnswerLabel.textColor = UIColor.greenColor()
        }
        else if QuestionHelper.UserAnswers[indexPath.row] != (-1){
            cell.StudentAnswerLabel.textColor = UIColor.redColor()
        }
        
        return cell
    }
    @IBAction func SonuclarClick(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "SolutionSegue"){
            let solutionViewController = segue.destinationViewController as! SolutionViewController
            solutionViewController.questionIndex = selectedRow
        }
        
    }
    

}
