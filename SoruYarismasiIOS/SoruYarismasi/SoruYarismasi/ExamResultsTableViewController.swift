//
//  ExamResultsTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 16/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class ExamResultsTableViewController: UITableViewController, ENSideMenuDelegate{
    var testHistories:[TestHistory] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        sideMenuController()?.sideMenu?.hideSideMenu()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        SwiftSpinner.show("Bilgileriniz Alınıyor..")
        ApiConnectionManager.sharedInstance.getJSON("GetTestHistory") { (json) -> Void in
            for var i=0;i<json.count;i++ {
                self.testHistories.append(TestHistory(json: json[i]))
            }
            self.tableView.reloadData()
            SwiftSpinner.hide()
        }
    }
    @IBAction func ToggleSideMenu(sender: UIBarButtonItem) {
        toggleSideMenuView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return testHistories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ExamResultsCell", forIndexPath: indexPath) as! ExamResultsTableViewCell
        let testHistory = testHistories[indexPath.row]
        let lid = testHistory.LectureId
        let tid = testHistory.TopicId
        let lecture = Lecture.findLectureById(lid, lectures: ApiContext.sharedInstance.Lectures)
        cell.DersKonuLabel.text = (lecture?.Name)!+" - "+(Topic.findTopicById(tid, topics: (lecture?.Topics)!)?.Name)!
        cell.SkorLabel.text = "Skor "+String(format:"%.2f",testHistory.Score)+" | Sıra \(testHistory.Siralama)"
        cell.YayineviLabel.text = testHistory.Publisher
        cell.TarihLabel.text = testHistory.MinutesPassed.toStringMinutesShortVersion()

        return cell
    }
      /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
