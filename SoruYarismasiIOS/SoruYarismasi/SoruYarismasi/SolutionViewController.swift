//
//  SolutionViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit
import AVFoundation
class SolutionViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var SoruCozumSegment: UISegmentedControl!
    @IBOutlet weak var CozumImageView: UIImageView!
    var questionIndex:Int!
    var gestureRec:UIGestureRecognizer!
    @IBOutlet weak var scrollView: UIScrollView!
    var shapes:[Shape] = []
    var questionImage:UIImage!
    var layerFlag = false
    var offsets:CGRect!
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        CozumImageView.userInteractionEnabled = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        SwiftSpinner.show("Çözüm Yükleniyor")
               var params:[String:String] = [:]
        params["questionid"] = String(QuestionHelper.testDetail.TestQuestionIds[questionIndex])
        params["testid"] = String(ApiContext.sharedInstance.ActiveTest.Id)
        params["mapid"] = String(QuestionHelper.testDetail.MapIds[questionIndex])
        if (QuestionHelper.Solutions[self.questionIndex] == nil){
            ApiConnectionManager.sharedInstance.getImage("GetSolution",parameters: params){(data) -> Void in
                QuestionHelper.Solutions[self.questionIndex] = data
                SwiftSpinner.hide()
                self.SoruCozumSegment.enabled = true
                if(self.SoruCozumSegment.selectedSegmentIndex == 2){
                    self.CozumImageView.image = UIImage(data:QuestionHelper.Solutions[self.questionIndex]!)
                }
            }
        }else{
            SwiftSpinner.hide()
        }
        questionImage = QuestionHelper.getQuestion("q_"+String(questionIndex))
        CozumImageView.image = questionImage

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func CevaplarClick(sender: UIBarButtonItem) {
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func SoruCozumSegmentControl(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            reset()
            CozumImageView.image = questionImage
            break
        case 1:
            reset()
            CozumImageView.image = questionImage
            refill()
            break;
        case 2:
            reset()
            CozumImageView.image = UIImage(data:QuestionHelper.Solutions[questionIndex]!)
            break
        default:
            break
        }
        scrollView.zoomScale = 1.0

    }
    func reset() {
        shapes.forEach{$0.caShape?.removeFromSuperlayer()}
        shapes.removeAll()
    }
    
    func refill() {
        offsets = AVMakeRectWithAspectRatioInsideRect(CozumImageView.image!.size,CozumImageView.frame)
        shapes.setPointsWithJsonString(QuestionHelper.ShapeJsonStrings[questionIndex],offsets: offsets)
        for shape in shapes{
            shape.caShape = CAShapeLayer()
            shape.caShape.path = shape.path.CGPath
            CozumImageView.layer.addSublayer(shape.caShape)
            EditModes(rawValue: shape.editMode)?.setEditOptions()
            shape.caShape.lineJoin = kCALineJoinMiter
            shape.caShape.opacity = EditOptions.sharedInstance.Opacity
            shape.caShape.lineWidth = EditOptions.sharedInstance.LineWidth
            shape.caShape.strokeColor = EditOptions.sharedInstance.Color.CGColor
            shape.caShape.fillColor = UIColor(hue: 0.786, saturation: 0.15, brightness: 0.89, alpha: 0.0).CGColor
        }
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return CozumImageView
    }
    func rotated(){
        scrollView.zoomScale = 1.0
        reset()
        refill()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
