//
//  TestDetail.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import Foundation
class TestDetail{
    var Publisher:String = ""
    var DurationInMinutes:Int = 0
    var Credit:Int = 0
    var TestQuestionIds:[Int] = []
    var MapIds:[Int] = []
    var Error:String = ""
    var Date:NSDate = NSDate()
    init(publisher:String,duration:Int,credit:Int,qids:JSON,mids:JSON,err:String){
        Publisher = publisher
        DurationInMinutes = duration
        Credit = credit
        for i in qids{
            TestQuestionIds.append(i.1.intValue)
        }
        for i in mids{
            MapIds.append(i.1.intValue)
        }
        Error = err
        Date = NSDate().dateByAddingTimeInterval(Double(DurationInMinutes)*60.0)
    }
    init(){
        
    }
    func isBigExam()->Bool{
        return Credit>10
    }
}