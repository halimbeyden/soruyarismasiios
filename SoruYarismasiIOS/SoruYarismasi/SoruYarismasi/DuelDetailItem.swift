//
//  DuelDetailItem.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 07/01/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class DuelDetailItem {
    var Username:String!
    var Id:Int!
    var TestId:Int!
    var TopicId:Int!
    var LectureId:Int!
    var OpponentScore:Float!
    var Score:Float!
    var MinutesPassed:Int!
    init(json:JSON){
        Username = json["Username"].stringValue
        Id = json["Id"].intValue
        TestId = json["TestId"].intValue
        TopicId = json["TopicId"].intValue
        LectureId = json["LectureId"].intValue
        OpponentScore = json["OpponentScore"].floatValue
        Score = json["Score"].floatValue
        MinutesPassed = json["MinutesPassed"].intValue
    }
}