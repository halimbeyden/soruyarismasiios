//
//  TestLobbyViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit

class TestLobbyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var PublisherLabel: UILabel!
    var counter:Int = 0
    @IBOutlet weak var RemainingTime: UILabel!
    var timer:NSTimer = NSTimer()
    var duelRequests:[DuelRequest] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        RemainingTime.text = ApiContext.sharedInstance.ActiveTest.Date.toStringLongVersion()
        PublisherLabel.text = ApiContext.sharedInstance.ActiveTest.Publisher
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        let components = ApiContext.sharedInstance.ActiveTest.Date.minus(NSDate(),unit: [.Second])
        counter = components.second
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        reloadData()
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section==0){
            return "Gelen Davetler"
        }
        else{
            return "Gönderilen Davetler"
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==0){
            return duelRequests.filter{(request) in
                    return request.IsReceived
                }.count
        }
        else{
            return duelRequests.filter{(request) in
                    return !request.IsReceived
                }.count

        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DuelCell", forIndexPath: indexPath) as! DuelTableViewCell
        if(indexPath.section == 0){
            var duels = duelRequests.filter{(request) in
                return request.IsReceived
                }
            cell.StudentNameLabel.text = duels[indexPath.row].StudentName
        }else{
            var duels = duelRequests.filter{(request) in
                return !request.IsReceived
            }
            cell.StudentNameLabel.text = duels[indexPath.row].StudentName
            cell.AcceptButton.hidden = true
        }
        return cell
    }
    
    func update(){
        if(counter<=0){
            timer.invalidate()
            let storyboard = UIStoryboard(name: "TestBoard", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("TestViewController") as! TestViewController
            self.presentViewController(vc, animated: true, completion: nil)

        }else{
            RemainingTime.text = ApiContext.sharedInstance.ActiveTest.Date.toStringLongVersion()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func AcceptButtonClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        var duels = duelRequests.filter{(request) in
            return request.IsReceived
        }
        yesNoDialog("Düello Onayı", message: "\(duels[(index?.row)!].StudentName) adlı kişinin düello davetini kabul etmek istiyor musunuz?", yesCallBack: { () -> Void in
                ApiConnectionManager.sharedInstance.getJSON("DuelAccept", parameters: ["duelid":duels[(index?.row)!].Id], callback: { (json) -> Void in
                    ApiConnectionManager.sharedInstance.jsonResultHandle(json, view: self)
                    self.reloadData()
                })
        })
    }
    @IBAction func RejectButtonClick(sender: UIButton) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        
        if(index?.section == 0){
            var duels = duelRequests.filter{(request) in
                return request.IsReceived
            }
            yesNoDialog("Düello İptali", message: "\(duels[(index?.row)!].StudentName) adlı kişinin düello davetini reddetmek istiyor musunuz?", yesCallBack: { () -> Void in
                ApiConnectionManager.sharedInstance.getJSON("DuelDecline", parameters: ["duelid":duels[(index?.row)!].Id], callback: { (json) -> Void in
                    ApiConnectionManager.sharedInstance.jsonResultHandle(json, view: self)
                    self.reloadData()
                })
            })
            
        }else{
            var duels = duelRequests.filter{(request) in
                return !request.IsReceived
            }
            yesNoDialog("Düello İptali", message: "\(duels[(index?.row)!].StudentName) adlı kişiye gönderdiğiniz düello davetini iptal etmek istiyor musunuz?", yesCallBack: { () -> Void in
                ApiConnectionManager.sharedInstance.getJSON("DuelCancelInvention", parameters: ["duelid":duels[(index?.row)!].Id], callback: { (json) -> Void in
                    ApiConnectionManager.sharedInstance.jsonResultHandle(json, view: self)
                    self.reloadData()
                })
            })
        }
    }
    func reloadData(){
        SwiftSpinner.show("Bilgiler Alınıyor")
        
        ApiConnectionManager.sharedInstance.getJSON("GetTestRequests") { (json) -> Void in
            self.duelRequests.removeAll()
            for var i = 0;i<json.count;i++ {
                self.duelRequests.append(DuelRequest(json: json[i]))
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
                SwiftSpinner.hide()
            })
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
