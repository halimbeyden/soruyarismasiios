//
//  ParentRequest.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 11/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class ParentRequest {
    var ParentId:Int! //String olması gerekmiyor mu
    var ParentName:String!
    var RequestId:Int!
    init(json:JSON){
        ParentId = json["ParentId"].intValue
        ParentName = json["ParentName"].stringValue
        RequestId = json["RequestId"].intValue
    }
}