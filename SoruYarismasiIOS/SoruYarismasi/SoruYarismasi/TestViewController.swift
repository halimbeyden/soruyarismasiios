//
//  TestViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 23/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit
import AVFoundation

class TestViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource,UIScrollViewDelegate, UIPopoverPresentationControllerDelegate, UITableViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var Loading: UIActivityIndicatorView!
    @IBOutlet weak var NextBtn: UIButton!
    @IBOutlet weak var BackBtn: UIButton!
    @IBOutlet weak var QuestionImage: UIImageView!
    @IBOutlet weak var OptionPicker: UIPickerView!
    @IBOutlet weak var QLabels: UIView!
    @IBOutlet weak var RemainingTime: UILabel!
    
    var timer:NSTimer = NSTimer()
    var counter:Int = 0
    var isTimerReady:Bool = false
    var activitiyViewController = ProgressResultsViewController(title: "Cevaplarınız Gönderiliyor..",rateExist: true)
    
    @IBAction func finishTestClick(sender: UIButton) {
        self.yesNoDialog("Sınavı Bitir", message: "Sınavı Bitirmek İstiyor musunuz?", yesCallBack: {
            self.finishTest()
            })
    }
    var pickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        QuestionImage.userInteractionEnabled = true
        QuestionImage.contentMode = .ScaleAspectFit
        pickerData = ["Boş", "A", "B", "C", "D", "E"]
        OptionPicker.delegate = self
        OptionPicker.dataSource = self
        Loading.startAnimating()
        var params:[String:String] = [:]
        params["testid"] = String(ApiContext.sharedInstance.ActiveTest.Id)
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        ApiConnectionManager.sharedInstance.getJSON("GetTestDetail",parameters: params) { (json) -> Void in
            QuestionHelper.testDetail = TestDetail(publisher: json["Publisher"].stringValue, duration: json["DurationInMinutes"].intValue, credit: json["Credit"].intValue, qids: json["TestQuestionIds"], mids: json["MapIds"], err: json["Error"].stringValue)
            QuestionHelper.ImageView = self.QuestionImage
            QuestionHelper.Loading = self.Loading
            QuestionHelper.Picker = self.OptionPicker
            QuestionHelper.QLabels = self.QLabels
            QuestionHelper.initialize()
            QuestionHelper.getQuestions(0)
            let components = QuestionHelper.testDetail.Date.minus(NSDate(),unit: [.Second])
            self.counter = components.second
            self.isTimerReady = true
        }
    }
    func update(){
        if(counter<=0 && isTimerReady){
            finishTest()
        }else{
            RemainingTime.text = QuestionHelper.testDetail.Date.toStringLongVersion()
            counter--
        }

    }
    func examEvaluateUpdate(){
        if(counter<0 && isTimerReady){
            timer.invalidate()
            var params:[String:AnyObject] = [:]
            params["TestId"] = String(ApiContext.sharedInstance.ActiveTest.Id)
            params["Rating"] = String(Int(activitiyViewController.getRating()))
            params["Answers"] = QuestionHelper.UserAnswers
            params["Reports"] = ""
            ApiConnectionManager.sharedInstance.getJSON("SendTestResults",parameters: params) { (json) -> Void in
                self.counter = QuestionHelper.ResponseTime
                dispatch_async(dispatch_get_main_queue(), {
                    self.dismissViewControllerAnimated(false, completion: { () -> Void in
                        self.activitiyViewController = ProgressResultsViewController(title: "Sonuçlar Değerlendiriliyor..",rateExist: true)
                        self.activitiyViewController.changeState(QuestionHelper.ResponseTime)
                        self.presentViewController(self.activitiyViewController, animated: false, completion: nil)
                        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("resultsUpdate"), userInfo: nil, repeats: true)
                    })
                })
            }
        }else{
            activitiyViewController.setProgress(Float(counter), message: counter.toStringSecondsToLongVersion())
            counter--
        }
    }
    func resultsUpdate(){
        if(counter<=0){
            activitiyViewController.setProgress(Float(counter), message: counter.toStringSecondsToLongVersion())
            timer.invalidate()
            var params:[String:String] = [:]
            params["testid"] = String(ApiContext.sharedInstance.ActiveTest.Id)
            SwiftSpinner.show("Lütfen Bekleyiniz..")
            ApiConnectionManager.sharedInstance.getJSON("GetTestResult",parameters: params) { (json) -> Void in
                self.activitiyViewController.dismissViewControllerAnimated(true, completion: {
                    QuestionHelper.testResult = TestResult(result: json)
                    let VC1 = self.storyboard!.instantiateViewControllerWithIdentifier("ResultsViewController") as! ResultsViewController
                    let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                    dispatch_async(dispatch_get_main_queue(), {
                        self.presentViewController(navController, animated:true, completion: nil)
                        SwiftSpinner.hide()
                    })
                })
            }
        }else{
            activitiyViewController.setProgress(Float(counter), message: counter.toStringSecondsToLongVersion())
            counter--
        }
        
    }
    func finishTest(){
        timer.invalidate()
        if QuestionHelper.testDetail.isBigExam(){
            counter = QuestionHelper.RatingTime
        }
        else{
            counter = QuestionHelper.testDetail.Date.minus(NSDate(), unit: [.Second]).second + QuestionHelper.RatingTime
        }
        QuestionHelper.ShapeJsonStrings[QuestionHelper.ActiveQuestion] = shapes.getJsonString(QuestionHelper.Offsets)
        presentViewController(activitiyViewController, animated: true, completion: nil)
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("examEvaluateUpdate"), userInfo: nil, repeats: true)
                activitiyViewController.setMaxProgress(counter)
    }
    override func viewWillAppear(animated: Bool) {
            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
    @IBAction func NextClick(sender: UIButton) {
        reset()
        QuestionHelper.loadQuestion(QuestionHelper.ActiveQuestion+1)
        self.scrollView.zoomScale = 1.0
        refill()
    }
    @IBAction func BackClick(sender: UIButton) {
        reset()
        QuestionHelper.loadQuestion(QuestionHelper.ActiveQuestion-1)
        self.scrollView.zoomScale = 1.0
        refill()
    }
    
    
    // Zoomdelegates:
    
    func zoom(tapGesture: UITapGestureRecognizer) {
        if (self.scrollView!.zoomScale == self.scrollView!.minimumZoomScale) {
            let center = tapGesture.locationInView(self.scrollView!)
            let size = self.QuestionImage!.image!.size
            let zoomRect = CGRectMake(center.x, center.y, (size.width / 2), (size.height / 2))
            self.scrollView!.zoomToRect(zoomRect, animated: true)
        } else {
            self.scrollView!.setZoomScale(self.scrollView!.minimumZoomScale, animated: true)
        }
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.QuestionImage
    }
    
    //Draw:
    var lastPoint = CGPoint.zero
   
    var swiped = false
    var path:UIBezierPath!
    var shape:CAShapeLayer!
    var eraseMode:Bool = false
    var shapes:[Shape] = []
    var ourShape:Shape!
    let eraserExtand:CGFloat = 40.0
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.locationInView(QuestionImage)
                if(editMode != .Eraser){
                    shape = CAShapeLayer()
                    
                    QuestionImage.layer.addSublayer(shape)
                    shape.opacity = EditOptions.sharedInstance.Opacity
                    shape.lineWidth = EditOptions.sharedInstance.LineWidth
                    shape.lineJoin = kCALineJoinMiter
                    shape.strokeColor = EditOptions.sharedInstance.Color.CGColor
                    shape.fillColor = UIColor(hue: 0.786, saturation: 0.15, brightness: 0.89, alpha: 0.0).CGColor
                    path = UIBezierPath()
                    ourShape = Shape()
                    ourShape.path = path
                    ourShape.caShape = shape
                    ourShape.editMode = editMode.rawValue
                    shapes.append(ourShape)
                    path.moveToPoint(lastPoint)
                }
        }
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.locationInView(QuestionImage)
            if(editMode == .Eraser){
                for sh in shapes{
                    if CGPathGetBoundingBox(sh.path.CGPath).expandRect(eraserExtand).contains(currentPoint){
                        let splittedShapes = sh.splitShape(currentPoint)
                        dispatch_async(dispatch_get_main_queue(), {
                            sh.caShape.removeFromSuperlayer()
                            self.shapes = self.shapes.filter{$0.caShape != sh.caShape}
                            if(splittedShapes.count > 0){
                                for ssh in splittedShapes{
                                    if(ssh.points.count > 0){
                                        let newShape = CAShapeLayer()
                                        ssh.caShape = newShape
                                        EditModes(rawValue: ssh.editMode)!.setEditOptions()
                                        newShape.opacity = EditOptions.sharedInstance.Opacity
                                        newShape.lineWidth = EditOptions.sharedInstance.LineWidth
                                        newShape.strokeColor = EditOptions.sharedInstance.Color.CGColor
                                        newShape.fillColor = UIColor(hue: 0.786, saturation: 0.15, brightness: 0.89, alpha: 0.0).CGColor
                                        let pt = UIBezierPath()
                                        pt.moveToPoint(ssh.points.first!)
                                        for (var i = 1;i < ssh.points.count;i++){
                                            pt.addLineToPoint(ssh.points[i])
                                        }
                                        newShape.path = pt.CGPath
                                        ssh.path = pt
                                        self.shapes.append(ssh)
                                        self.QuestionImage.layer.addSublayer(newShape)
                                    }
                                }
                            }
                        })
                    }
                }
            }else{
                path.addLineToPoint(currentPoint)
                shape.path = path.CGPath
                ourShape.path = path
                ourShape.points.append(currentPoint)
            }
            lastPoint = currentPoint
        }
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if !swiped {

        }
    }
    func reset() {
        QuestionHelper.ShapeJsonStrings[QuestionHelper.ActiveQuestion] = shapes.getJsonString(QuestionHelper.Offsets)
        shapes.forEach{$0.caShape.removeFromSuperlayer()}
        shapes.removeAll()
    }

    func refill() {
        shapes.setPointsWithJsonString(QuestionHelper.ShapeJsonStrings[QuestionHelper.ActiveQuestion],offsets: QuestionHelper.Offsets)
        for shape in shapes{
            shape.caShape = CAShapeLayer()
            shape.caShape.path = shape.path.CGPath
            QuestionImage.layer.addSublayer(shape.caShape)
            editMode = EditModes(rawValue: shape.editMode)!
            editMode.setEditOptions()
            shape.caShape.lineJoin = kCALineJoinMiter
            shape.caShape.opacity = EditOptions.sharedInstance.Opacity
            shape.caShape.lineWidth = EditOptions.sharedInstance.LineWidth
            shape.caShape.strokeColor = EditOptions.sharedInstance.Color.CGColor
            shape.caShape.fillColor = UIColor(hue: 0.786, saturation: 0.15, brightness: 0.89, alpha: 0.0).CGColor
        }
    }

    @IBOutlet weak var zoomMode: UIButton!
    @IBOutlet weak var drawMode: UIButton!
    var editMode:EditModes = EditModes.Zoom
    @IBAction func zoomMode(sender: UIButton) {
        scrollView.scrollEnabled = true
        scrollView.userInteractionEnabled = true
        dispatch_async(dispatch_get_main_queue(), {
            self.editMode = EditModes.Zoom
            self.drawMode.setImage(UIImage(named: "image_edit"), forState: .Normal)
        })
    }
    @IBAction func drawMode(sender: UIButton) {
        self.performSegueWithIdentifier("editMenuSegue", sender: self)
    }
    func rotated(){
        scrollView.zoomScale = 1.0
        reset()
        QuestionHelper.Offsets = AVMakeRectWithAspectRatioInsideRect(QuestionImage.image!.size,QuestionImage.frame)
        refill()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    var popController:UITableViewController!
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "editMenuSegue"{
            popController = segue.destinationViewController as! UITableViewController
            popController.tableView.delegate = self
            if let controller = popController.popoverPresentationController{
               controller.delegate = self
            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        debugPrint(indexPath.row)
        dispatch_async(dispatch_get_main_queue(), {
            self.editMode = EditModes.init(rawValue: indexPath.row + 1)!
            self.drawMode.setImage(UIImage(named: (self.editMode.ImageName)), forState: .Normal)
            self.drawMode.setImage(UIImage(named: (self.editMode.ImageName)), forState: .Selected)
            self.popController.dismissViewControllerAnimated(true, completion: nil)
            self.scrollView.scrollEnabled = false
            self.scrollView.userInteractionEnabled = false
            self.editMode.setEditOptions()
        })
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .Popover
    }
    }
class EditOptions {
    static let sharedInstance:EditOptions = EditOptions()
    var Opacity:Float = 0.7
    var Color:UIColor = UIColor(red: 220, green: 42, blue: 23, alpha: 1.0)
    var LineWidth:CGFloat = 2
}
enum EditModes:Int{
    case Zoom = 0
    case Pencil
    case RedPencil
    case MarkerPen
    case Eraser
    var ImageName: String {
        switch self {
        case Zoom:
            return "zoom"
        case Pencil:
            return "edit"
        case .RedPencil:
            return "redpencil"
        case .MarkerPen:
            return "markerpencil"
        case .Eraser:
            return "eraser"
        }
    }
    func setEditOptions()->Void {
        switch self {
        case Pencil:
            EditOptions.sharedInstance.Opacity = 0.6
            EditOptions.sharedInstance.Color = UIColor(red: 0.24, green: 0.64, blue: 0.64, alpha: 1.0)
            
            EditOptions.sharedInstance.LineWidth = 2
        case .RedPencil:
            EditOptions.sharedInstance.Opacity = 0.6
            EditOptions.sharedInstance.Color = UIColor(red: 0.88, green: 0.16, blue: 0.09, alpha: 1.0)
            EditOptions.sharedInstance.LineWidth = 2
        case .MarkerPen:
            EditOptions.sharedInstance.Opacity = 0.3
            EditOptions.sharedInstance.Color = UIColor(red: 0.94, green: 0.94, blue: 0.2, alpha: 1.0)
            EditOptions.sharedInstance.LineWidth = 20
        default:
            return
        }
    }
}

