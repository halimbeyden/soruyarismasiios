//
//  TestTableViewCell.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 18/12/15.
//  Copyright © 2015 Pinnera. All rights reserved.
//

import UIKit

class TestTableViewCell: UITableViewCell {

    @IBOutlet weak var PublisherLabel: UILabel!
    @IBOutlet weak var RemainingTimeLabel: UILabel!
    @IBOutlet weak var CreditLabel: UILabel!
    
    @IBOutlet weak var AjandayaEkle: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
