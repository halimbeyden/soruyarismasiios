//
//  Shape.swift
//  PolygonDeneme
//
//  Created by Halim Beyden on 15/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
import UIKit
class Shape {
    var path:UIBezierPath!
    var caShape:CAShapeLayer!
    var points:[CGPoint] = []
    var editMode:Int = 1
    init(){
        
    }
    init(json:JSON,offsets:CGRect){
        points = json["points"].arrayValue.map{ p -> (CGPoint) in
            offsets.convertFromRegressedPoint(CGPoint(x:p["x"].doubleValue,y:p["y"].doubleValue))
        }
        editMode = json["editMode"].intValue
        path = UIBezierPath()
        if(points.count>0){
            path.moveToPoint((points.first!))
            for(var i = 1;i<points.count;i++){
                path.addLineToPoint((points[i]))
            }
        }
    }
    func getRegressedPoints(rect:CGRect)->[CGPoint]{
        return points.map{rect.convertReggessedPoint($0)}
    }
    func getPointsInRange(point:CGPoint)->[CGPoint]{
        let rect = CGRectMake(point.x - 20, point.y - 20, point.x + 20, point.y + 20)
        return points.filter{ fpoint in
            rect.contains(fpoint)
        }
    }
    func splitShape(point:CGPoint)->[Shape]{
        let rect = CGRectMake(point.x - 16, point.y - 16, 32,32)
        var shapes:[Shape] = []
        var currentShape:Shape = Shape()
        for var i = 0;i < points.count;i++ {
            let point = points[i]
            if(rect.contains(point)){
                if(currentShape.points.count>1){
                    currentShape.editMode = self.editMode
                    shapes.append(currentShape)
                }
                if(currentShape.points.count>0){
                    currentShape = Shape()
                }
            }else{
                currentShape.points.append(point)
            }
        }
         if(currentShape.points.count>1){
            currentShape.editMode = self.editMode
            shapes.append(currentShape)
        }
        return shapes
    }
    func getJsonString(rect:CGRect)->String{
        let pointsForJSON = getRegressedPoints(rect).map{"{\"x\": \($0.x), \"y\": \($0.y)}"}.joinWithSeparator(",")
        return "{\"points\":[" + pointsForJSON + "], \"editMode\":\(self.editMode)}"
    }
}
extension _ArrayType where Generator.Element == Shape {
    func getJsonString(offsets:CGRect)->String{
        return "[" + self.map{$0.getJsonString(offsets)}.joinWithSeparator(",") + "]"
    }
    mutating func setPointsWithJsonString(stringJSON:String,offsets:CGRect){
        if let dataFromString = (stringJSON).dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            for var i = 0;i < json.count;i++ {
                self.append(Shape(json: json[i],offsets:offsets))
            }
        }
        
    }
}
