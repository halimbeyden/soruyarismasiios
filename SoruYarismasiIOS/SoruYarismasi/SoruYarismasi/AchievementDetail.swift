//
//  AchievementDetail.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 12/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import Foundation
class AchievementDetail {
    var StudentAchievements:[Int] = []
    var Achievements:[Achievement] = []
    init(json:JSON){
        StudentAchievements = json["StudentAchievements"].arrayValue.map{$0.intValue}
        Achievements = json["Achievements"].arrayValue.map{Achievement(json:$0)}
    }
}