//
//  ParentTableViewController.swift
//  SoruYarismasi
//
//  Created by Halim Beyden on 11/02/16.
//  Copyright © 2016 Pinnera. All rights reserved.
//

import UIKit

class ParentTableViewController: UITableViewController {

    var parents:[Parent] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController()?.sideMenu?.hideSideMenu()
        SwiftSpinner.show("Evebeyn Listesi Alınıyor..")
        ApiConnectionManager.sharedInstance.getJSON("Parents", callback: {
            (json)->Void in
            for(var i = 0;i<json.count;i++){
                self.parents.append(Parent(json: json[i]))
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
                SwiftSpinner.hide()
            })
        })
        ApiConnectionManager.sharedInstance.getJSON("ParentRequests", callback: {
            (json)->Void in
            for(var i = 0;i<json.count;i++){
                let request = ParentRequest(json: json[i])
                var params:[String:String] = [:]
                params["requestid"] = String(request.RequestId)
                dispatch_async(dispatch_get_main_queue(), {
                    self.yesNoDialog("Evebeyn İsteği", message: "\(request.ParentName) adlı kullanıcının evebeyniniz olduğunu doğruluyor musunuz?",
                        yesCallBack: {
                        ApiConnectionManager.sharedInstance.getJSON("AcceptParent",parameters: params, callback: {
                            (json)->Void in
                            
                        })
                        
                        }, noCallBack: {
                        ApiConnectionManager.sharedInstance.getJSON("DeclineParent",parameters: params, callback: {
                            (json)->Void in
                            dispatch_async(dispatch_get_main_queue(), {
                                self.tableView.reloadData()
                            })
                            debugPrint(json)
                            })
                        })
                    })
                }
            
        })
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func CancelParentClick(sender: AnyObject) {
        let btnPos = sender.convertPoint(CGPointZero, toView:self.tableView)
        let index = self.tableView.indexPathForRowAtPoint(btnPos)
        yesNoDialog("Evebeyn İptali", message: "Bu kişiyi evebeynleriniz arasından kaldırmak istiyor musunuz?", yesCallBack: {
            SwiftSpinner.show("Lütfen bekleyiniz..")
            var params:[String:String] = [:]
            params["parenthoodid"] = String(self.parents[(index?.row)!].ParenthoodId)
            ApiConnectionManager.sharedInstance.getJSON("CancelParent",parameters: params, callback: {
                (json)->Void in
                self.parents.removeAtIndex((index?.row)!)
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                    SwiftSpinner.hide()
                })
            })
        })
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return parents.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ParentCell", forIndexPath: indexPath) as! ParentTableViewCell
        cell.ParentName.text = parents[indexPath.row].ParentName
        // Configure the cell...
        
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
